![Boot.Mvc.Templating.png](res/Boot.Mvc.Templating.png)
# Welcome to Boot.Mvc.Templating

###This project is a part of the Boot Project. Boot.Mvc.Templating makes it easy to use Regions to place content, regulary by using a widget or module system, which also is built in.

###Core functionality
The templating system uses a modern layout from code where you can predefine templates for use in your system.

##Define a template

Add namespace to web.config below Views.

     <pages pageBaseType="Boot.Mvc.Templating.Controlbase.ViewPage">
      <namespaces>
         <add namespace="Boot.Mvc.Templating" />
         ....

1. Call template manager by Html.BootLayout()
2. Add a Row
3. Add a Cell
4. Add Zone to Cell
4. Add Region to Zone
5. The AddZone statement renders all widget in this Zone.

#

    @using (var b = Html.BootLayout().Begin(new Layout("_Startpage").CssClass("container")))
    {
    <!--
        Template: Startpage
        A typical startpage of bootstrap layout.
    -->
    
    //Header
    using (var row = b.AddRow()){
        using (var c = row.AddCell(CellSpace.L, 12)) {
            c.AddZone(Region.Header);
        }
    }

    //Triple columns
    using (var row = b.AddRow())
    {
        using (var c = row.AddCell(CellSpace.L, 4)){
            c.AddZone(Region.TripelFirst);
        }
        using (var c = row.AddCell(CellSpace.L, 4)){
            c.AddZone(Region.TripelSecond);
        }
        using (var c = row.AddCell(CellSpace.L, 4)){
            c.AddZone(Region.TripelThird);
        }
    }

    //Footer
    using (var row = b.AddRow()) {
        using (var c = row.AddCell(CellSpace.L, 12)) {
            c.AddZone(Region.Footer);
        }
    }
    }


### The output from above code:

    <div class="container">
      <div class="row">
          <div class="col-lg-12">
              <div class="mywidget">Header</div>
          </div>
      </div>
      <div class="row">
          <div class="col-lg-4">
              <div class="mywidget">TripelFirst</div><!--Your widget, can be multiple inside region.-->
          </div>
          <div class="col-lg-4">
              <div class="mywidget">TripelSecond</div>
          </div>
          <div class="col-lg-4">
              <div class="mywidget">TripelThird</div>
          </div>
      </div>
      <div class="row">
          <div class="col-lg-12">
              <div class="mywidget">Footer</div>
          </div>
      </div>
    </div>