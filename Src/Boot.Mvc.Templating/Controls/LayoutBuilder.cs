﻿using Boot.Mvc.Templating.Infrastructure;
using System;
using System.Web.Mvc;


namespace Boot.Mvc.Templating
{
    public class LayoutBuilder<TModel> : BuilderBase<TModel, Layout>
    {
        HtmlHelper html;
        internal LayoutBuilder(HtmlHelper<TModel> htmlHelper, Layout modal)
            : base(htmlHelper, modal)
        {
            this.html = htmlHelper;
        }


        public SectionPanel BeginContainer()
        {
            return new SectionPanel(html, Section.Container, base.textWriter);
        }


        /// <summary>
        /// Adds a regular div.
        /// </summary>
        /// <returns>RowSection</returns>
        public RowSection AddRow()
        {
            return new RowSection(html, base.textWriter);
        }


        /// <summary>
        /// Adds a regular div.
        /// </summary>
        /// <returns>RowSection</returns>
        public RowSection AddRow(string cssClass)
        {
            return new RowSection(html, base.textWriter, cssClass);
        }

        /// <summary>
        /// Adds a cell.
        /// </summary>
        /// <param name="space">The space to use, S,M,L (col-sm-, col-md, col-lg)</param>
        /// <param name="count">Used for parameter space, eg. (col-sm-8)</param>
        /// <returns></returns>
        public RowSection AddCell(CellSpace space, int count)
        {
            return new RowSection(html, base.textWriter);
        }


        /// <summary>
        /// Adds a jumbotron and render Controls in this Region.
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        public SectionPanel AddZone(Region region) 
        {
            return new SectionPanel(html, Section.Region, region, textWriter, true);
        }

        /// <summary>
        /// Adds a div, render Controls in this Region.
        /// </summary>
        /// <param name="region"></param>
        /// <param name="cssClass"></param>
        /// <returns></returns>
        public SectionPanel AddZone(Region region, string cssClass)
        {
            return new SectionPanel(html, Section.Region, region, textWriter, cssClass);
        }
    }
}
