﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Web.Mvc;
using System.Linq;
using Helper = Boot.Mvc.Templating.HtmlHelperExtensions;

namespace Boot.Mvc.Templating
{
    public class SectionPanel : IDisposable
    {

        private readonly TextWriter textWriter;
        private readonly HtmlHelper html;
    

        internal SectionPanel(HtmlHelper html, TextWriter writer)
        {
            this.html = html;
            this.textWriter = writer;
        }


        internal SectionPanel(HtmlHelper html, Section section, TextWriter writer)
        {
            this.textWriter = writer;

            switch (section)
            {
                case Section.Container: this.textWriter.WriteLine(@"<div class=""container"">\n"); break;
                case Section.Row: this.textWriter.WriteLine(@"\t\t<div class=""row"">"); break;
            }
        }


        /// <summary>
        /// Creates a jumbotron.
        /// Closes div immediately after render.
        /// </summary>
        /// <param name="html"></param>
        /// <param name="section"></param>
        /// <param name="region"></param>
        /// <param name="writer"></param>
        /// <param name="p"></param>
        internal SectionPanel(HtmlHelper html, Section section, Region region, TextWriter writer, bool p)
        {
            this.html = html;
            this.textWriter = writer;
            this.textWriter.WriteLine(@"<div class=""jumbotron"">");

            dynamic model = html.ViewDataContainer.ViewData.Model; //Grab ViewModel
            var controls = (List<IControl>)model.Controls; //Extract all controls.
            RenderZone(controls, region); //Render
            Dispose();
        }

        //test
        internal SectionPanel(HtmlHelper html, Section section, Region region, TextWriter writer, Region regionTest)
        {
            this.html = html;
            this.textWriter = writer;

            this.textWriter.WriteLine(string.Format("\t\t\t<!--w--><div class=\"test\">{0}</div><!--//w-->", regionTest.ToString())); 
        }



        /// <summary>
        /// Creates a div tag.
        /// Closes div immediately after render.
        /// </summary>
        /// <param name="html"></param>
        /// <param name="section"></param>
        /// <param name="region"></param>
        /// <param name="writer"></param>
        /// <param name="p"></param>
        internal SectionPanel(HtmlHelper html, Section section, Region region, TextWriter writer, string cssClass)
        {
            this.html = html;
            this.textWriter = writer;
            this.textWriter.WriteLine(string.Format(@"\t<div class=""{0}"">", cssClass));

            dynamic model = html.ViewDataContainer.ViewData.Model; //Grab ViewModel
            var controls = (List<IControl>)model.Controls; //Extract all controls.
            RenderZone(controls, region); //Render
            Dispose();
        }


        /// <summary>
        /// SectionPanel
        /// </summary>
        /// <param name="html"></param>
        /// <param name="section"></param>
        /// <param name="region">Region Region</param>
        /// <param name="writer">TextWriter writer</param>
        internal SectionPanel(HtmlHelper html, Section section, Region region, TextWriter writer)
        {
            this.html = html;
            this.textWriter = writer;

            dynamic model = html.ViewDataContainer.ViewData.Model; //Grab ViewModel
            var controls = (List<IControl>)model.Controls; //Extract all controls.
            RenderZone(controls, region); //Render
        }




        /// <summary>
        /// Render controls in a specific zone.
        /// </summary>
        /// <param name="controls">The controls to render</param>
        /// <param name="region">Region to render controls into.</param>
        private void RenderZone(List<IControl> controls, Region region)
        {
            (from listofcontrols in controls 
               select listofcontrols)
                .ToList()
                  .ForEach(control => {
                     if(control.Region == region)
                       this.textWriter.Write(Helper.RenderControl(html, control.ViewPath, control));  
            });
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void Dispose()
        {
            this.textWriter.WriteLine("</div>\n");
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ToString() { return ""; /* return base.ToString(); */ }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Equals(object obj) { return base.Equals(obj); }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int GetHashCode() { return base.GetHashCode(); }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public new Type GetType() { return base.GetType(); }
    }
}
