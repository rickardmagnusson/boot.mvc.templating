﻿using Boot.Mvc.Templating.Infrastructure;
using System;
using System.Collections.Generic;

namespace Boot.Mvc.Templating
{

    public class Layout : HtmlElement
    {

        public Layout() : base("div")
        {
        }

        public Layout(string templateName) : base("div")
        {
        }

        public Layout Id(string id)
        {
            MergeHtmlAttribute("id", id);
            return this;
        }

        public Layout CssClass(string id)
        {
            MergeHtmlAttribute("class", id);
            return this;
        }

        public Layout HtmlAttributes(IDictionary<string, object> htmlAttributes)
        {
            SetHtmlAttributes(htmlAttributes);
            return this;
        }

        public Layout HtmlAttributes(object htmlAttributes)
        {
            SetHtmlAttributes(htmlAttributes);
            return this;
        }
    }
}
