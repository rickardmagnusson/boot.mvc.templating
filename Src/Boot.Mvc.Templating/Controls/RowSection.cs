﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Web.Mvc;

namespace Boot.Mvc.Templating
{
    public class RowSection : IDisposable
    {
        private readonly TextWriter textWriter;
        private readonly HtmlHelper html;

        internal RowSection(HtmlHelper html, TextWriter writer)
        {
            this.html = html;
            this.textWriter = writer;
            this.textWriter.WriteLine("\t<div class=\"row\">");
        }

        internal RowSection(HtmlHelper html, TextWriter writer, string cssClass)
        {
            this.html = html;
            this.textWriter = writer;
            this.textWriter.WriteLine(string.Format("\t<div class=\"row {0}\">", cssClass));
        }

        public CellSection InWell(CellSpace space, int count, bool jumbotron) //Render a region
        {
            return new CellSection(html, space, count, textWriter);
        }


        public CellSection AddCell(CellSpace space, int count) //Render a region
        {
            return new CellSection(html, space, count, textWriter);
        }

        public CellSection AddCell(CellSpace space, int count, string cssClass) //Render a region
        {
            return new CellSection(html, space, count, textWriter, cssClass);
        }


        [EditorBrowsable(EditorBrowsableState.Never)]
        public void Dispose()
        {
#if DEBUG
            this.textWriter.WriteLine("\t</div><!--//row-->\n");
#else
            this.textWriter.WriteLine("\t</div>\n");
#endif
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ToString() { return ""; /* return base.ToString(); */ }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Equals(object obj) { return base.Equals(obj); }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int GetHashCode() { return base.GetHashCode(); }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public new Type GetType() { return base.GetType(); }
    }
}
