﻿using System;
using System.ComponentModel;
using System.IO;
using System.Web.Mvc;


namespace Boot.Mvc.Templating
{
    public class CellSection : IDisposable
    {
        private readonly TextWriter textWriter;
        private readonly HtmlHelper html;
        internal CellSection(HtmlHelper html, CellSpace space, int count, TextWriter writer)
        {
            this.html = html;
            this.textWriter = writer;

            switch (space)
            {
                case CellSpace.S: this.textWriter.WriteLine(string.Format("\t\t<div class=\"col-sm-{0}\">", count)); break;
                case CellSpace.M: this.textWriter.WriteLine(string.Format("\t\t<div class=\"col-md-{0}\">", count)); break;
                case CellSpace.L: this.textWriter.WriteLine(string.Format("\t\t<div class=\"col-lg-{0}\">", count)); break;
            }
        }


        internal CellSection(HtmlHelper html, CellSpace space, int count, TextWriter writer, string cssClass)
        {
            this.html = html;
            this.textWriter = writer;

            switch (space)
            {
                case CellSpace.S: this.textWriter.WriteLine(string.Format("\t\t<div class=\"col-sm-{0} {1}\">", count, cssClass)); break;
                case CellSpace.M: this.textWriter.WriteLine(string.Format("\t\t<div class=\"col-md-{0} {1}\">", count, cssClass)); break;
                case CellSpace.L: this.textWriter.WriteLine(string.Format("\t\t<div class=\"col-lg-{0} {1}\">", count, cssClass)); break;
            }
        }

        public SectionPanel AddZone(Region region) //Render a region
        {
            return new SectionPanel(html, Section.Region, region, textWriter);
        }

        public SectionPanel AddTest(Region region) //Render a region
        {
            return new SectionPanel(html, Section.Region, region, textWriter, region);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void Dispose()
        {

#if DEBUG
            this.textWriter.WriteLine("\t\t</div><!--//column-->");
#else
            this.textWriter.WriteLine("\t</div>\n");
#endif
            
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ToString() { return ""; /* return base.ToString(); */ }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Equals(object obj) { return base.Equals(obj); }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int GetHashCode() { return base.GetHashCode(); }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public new Type GetType() { return base.GetType(); }



    }
}
