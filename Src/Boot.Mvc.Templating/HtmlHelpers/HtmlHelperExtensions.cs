﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Boot.Mvc.Templating
{
    public static partial class HtmlHelperExtensions
    {
        public static string RenderControl<T>(this HtmlHelper html, string viewPath, T model)
        {
            try
            {
                using (var writer = new StringWriter())
                {
                    var view = new WebFormView(html.ViewContext.Controller.ControllerContext, viewPath);
                    var vdd = new ViewDataDictionary<T>(model);
                    var context = new ViewContext(html.ViewContext.Controller.ControllerContext, view, vdd, new TempDataDictionary(), writer);
                    var v = new RazorView(html.ViewContext.Controller.ControllerContext, viewPath, null, false, null);
                    v.Render(context, writer);

                    return writer.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
