﻿using System;
using System.Web.Mvc;

namespace Boot.Mvc.Templating
{
    public static class BootLayoutHtmlExtension
    {
        public static BootLayout<TModel> BootLayout<TModel>(this HtmlHelper<TModel> helper)
        {
            return new BootLayout<TModel>(helper);
        }
    }
}
