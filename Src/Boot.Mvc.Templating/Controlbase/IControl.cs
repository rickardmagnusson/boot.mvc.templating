﻿using System;

namespace Boot.Mvc.Templating
{
    public interface IControl
    {
        Region Region { get; set; }
        string ViewPath { get; }
    }
}
