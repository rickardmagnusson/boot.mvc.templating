﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml;

namespace Boot.Mvc.Templating
{
    /// <summary>
    /// Base class of pages. Contain HtmlExtension helpers.
    /// </summary>
    /// <typeparam name="TModel">A dynamic model as strongly typed class</typeparam>
    public abstract class ViewPage<TModel> : System.Web.Mvc.WebViewPage<TModel>
    {

        private static ViewContext context;
        private static IViewDataContainer container;
        private readonly HtmlHelperFactory factory = new HtmlHelperFactory();
        public static HtmlHelper<TModel> Html { get; private set; }


        private void CreateHelper(TModel model)
        {
            Html = factory.CreateHtmlHelper(model, new StringWriter(new StringBuilder()));
        }


        internal virtual TModel BaseModel
        {
            get { return base.Model; }
            set { CreateHelper(value); }
        }


        /// <summary>
        /// Initialize helpers
        /// </summary>
        public override void InitHelpers()
        {
            base.InitHelpers();
            context = ViewContext;
            container = this;
            Html = new HtmlHelper<TModel>(context, container);
        }


        #region Helpers


        /// <summary>
        /// BootLayout webpage helper.
        /// </summary>
        /// <returns>BootLayout</returns>
        public static BootLayout<TModel> BootLayout()
        {
            return Html.BootLayout<TModel>();
        }


        #endregion
    }
}
