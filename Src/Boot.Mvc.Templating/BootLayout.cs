﻿using System;
using System.Web.Mvc;

namespace Boot.Mvc.Templating
{
    public partial class BootLayout<TModel>
    {
        public HtmlHelper<TModel> Html;

        public BootLayout(HtmlHelper<TModel> _html)
        {
            this.Html = _html;
        }
    }

    public partial class BootLayout<TModel>
    {
        public LayoutBuilder<TModel> Begin(Layout Layout)
        {
            return new LayoutBuilder<TModel>(Html, Layout);
        }
    }
}
