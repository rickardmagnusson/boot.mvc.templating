﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boot.Mvc.Templating
{
    /// <summary>
    /// Predefined region for Boot Project.
    /// Add more if needed.
    /// Taken from Orchard Cms
    /// </summary>
    public enum Region
    {
        Header = 0,
        Navigation = 1,
        Featured = 2,
        BeforeMain = 3,
        AsideFirst = 4,
        Messages = 5,
        BeforeContent = 6,
        Content = 7,
        AfterContent = 8,
        AsideSecond = 9,
        AfterMain = 10,
        TripelFirst = 11,
        TripelSecond = 12,
        TripelThird = 13,
        FooterQuadFirst = 14,
        FooterQuadSecond = 15,
        FooterQuadThird = 16,
        FooterQuadFourth = 17,
        Footer = 18,
        Sidebar,
        Topmenu,
        SidebarMenu,
        Cell
    }


    public enum CellSpace
    {
        S, M, L
    }


    internal enum Section
    {
        Container,
        Region,
        Row,
        Cell
    }
}
