﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Imports by Template system
using Boot.Mvc.Templating;
using Boot.Templating.Web.ViewModels;

namespace Boot.Templating.Web.Controllers
{
    public class HomeController : Controller
    {
        PageViewModel Model { get; set; }
        
        public ActionResult Index(string id)
        {
            string pid = (id == "") ? "/" : id;

            Model = new PageViewModel();
            Model.Page = Model.Pages.Single(p => p.Id.Equals(pid.ToLower()));
            
            Model.Controls.AddRange(Model.Contents.FindAll(c => c.PageId.Equals(Model.Page.Id)));
            Model.Controls.AddRange(Model.Contacts.FindAll(c => c.PageId.Equals(Model.Page.Id)));
            Model.Controls.AddRange(Model.ContactForms.FindAll(c => c.PageId.Equals(Model.Page.Id)));

            return View(Model.Page.Template, Model);
        }
    }
}