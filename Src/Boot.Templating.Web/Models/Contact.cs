﻿using Boot.Mvc.Templating;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boot.Templating.Web.Models
{
    public class Contact : IControl
    {
        public string Id { get; set; }
        public string PageId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Region Region { get; set; }


        public string ViewPath
        {
            get { return "~/Views/Controls/Contact.cshtml"; }
        }
    }
}