﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boot.Templating.Web.Models
{
    public class Page
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Template { get; set; }
    }
}