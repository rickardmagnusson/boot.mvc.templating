﻿using Boot.Mvc.Templating;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boot.Templating.Web.Models
{
    public class ContactForm : IControl
    {
        public string Id { get; set; }
        public string PageId { get; set; }
        public Region Region { get; set; }

        public string ViewPath
        {
            get { return "~/Views/Controls/ContactForm.cshtml"; }
        }
    }
}