﻿using Boot.Mvc.Templating;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boot.Templating.Web.Models
{
    public class Content : IControl
    {
        public string Id { get; set; }
        public string PageId { get; set; }
        public string Title { get; set; }
        public string Html { get; set; }
        public Region Region { get; set; }


        public string ViewPath
        {
            get { return "~/Views/Controls/Content.cshtml"; }
        }
    }
}