﻿using Boot.Mvc.Templating;
using Boot.Templating.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boot.Templating.Web.ViewModels
{
    public class PageViewModel
    {
        public Page Page { get; set; }
        public Content Content { get; set; }
        public List<Page> Pages { get; set; }
        public List<Content> Contents { get; set; }
        public List<Contact> Contacts { get; set; }
        public List<ContactForm> ContactForms { get; set; }
        public List<IControl> Controls = new List<IControl>();

        public PageViewModel()
        {
            Pages = new List<Page>();
            Pages.Add(new Page { Id = "/", Title = "Home", Template = "~/Views/Templates/_Startpage.cshtml" });
            Pages.Add(new Page { Id = "about", Title = "About", Template = "~/Views/Templates/_Two-Columns.cshtml" });
            Pages.Add(new Page { Id = "contact", Title = "Contact", Template = "~/Views/Templates/_Two-Columns.cshtml" });
            Pages.Add(new Page { Id = "test", Title = "Test", Template = "~/Views/Templates/_all.cshtml" });

            Contents = new List<Content>();
            //Home page
            Contents.Add(new Content { Id = "1", PageId = "/", Title = "First", Region = Region.TripelFirst, Html = "<h2>First</h2><p>This is the first content</p>" });
            Contents.Add(new Content { Id = "2", PageId = "/", Title = "Second", Region = Region.TripelSecond, Html = "<h2>Second</h2><p>This is the second content</p>" });
            Contents.Add(new Content { Id = "3", PageId = "/", Title = "Third", Region = Region.TripelThird, Html = "<h2>Third</h2><p>This is the third content</p>" });
            Contents.Add(new Content { Id = "4", PageId = "/", Title = "Boot templating header", Region = Region.Header, Html = "<p>This is the header content</p>" });
            Contents.Add(new Content { Id = "5", PageId = "/", Title = "Boot Project", Region = Region.Footer, Html = "<p>&copy; All rights reserved, Boot Project 2014.</p>" });
            Contents.Add(new Content { Id = "6", PageId = "/", Title = "Boot Project 2014", Region = Region.BeforeMain, Html = "<h1>Boot Project 2014</h1><p>Boot.Mvc.Templating makes templating easy.</p>" });
            
            //about page
            Contents.Add(new Content { Id = "7", PageId = "about", Title = "About", Region = Region.AsideFirst, Html = "<h2>About</h2><p>This is the about content</p>" });
            
            //contact page
            Contents.Add(new Content { Id = "8", PageId = "contact", Title = "About", Region = Region.AsideFirst, Html = "<h2>Contact</h2><p>This is the contact content</p>" });
            
            Contacts = new List<Contact>();
            Contact contact = new Contact { Id = "1", PageId="contact", Name="Rickard Magnusson", Email="rickard@bootproject.com", Phone="0722 56 56 56", Region=Region.Content };
            Contact contact2 = new Contact { Id = "2", PageId = "contact", Name = "Kajsa Magnusson", Email = "kajsa@bootproject.com", Phone = "0703 89 89 89", Region = Region.Content };
            Contacts.Add(contact);
            Contacts.Add(contact2);

            ContactForms = new List<ContactForm>();
            ContactForm form = new ContactForm { Id = "1", PageId = "contact", Region = Region.Content };
            ContactForms.Add(form);

            //Test page contents
            Contents.Add(new Content { Id = "9", PageId = "test", Title = "Header", Region = Region.Header, Html = "<h2>Header</h2><p>Header content</p>" });
            
            Contents.Add(new Content { Id = "10", PageId = "test", Title = "Navigation", Region = Region.Navigation, Html = "<p>Navigation content</p>" });
            Contents.Add(new Content { Id = "11", PageId = "test", Title = "BeforeMain", Region = Region.BeforeMain, Html = "<p>BeforeMain content</p>" });
            Contents.Add(new Content { Id = "12", PageId = "test", Title = "Featured", Region = Region.Featured, Html = "<p>Featured content</p>" });
            
            Contents.Add(new Content { Id = "13", PageId = "test", Title = "AsideFirst", Region = Region.AsideFirst, Html = "<h2>AsideFirst</h2><p>AsideFirst content</p>" });
            Contents.Add(new Content { Id = "14", PageId = "test", Title = "AsideSecond", Region = Region.AsideSecond, Html = "<h2>AsideSecond</h2><p>AsideSecond content</p>" });
           
            Contents.Add(new Content { Id = "15", PageId = "test", Title = "Messages", Region = Region.Messages, Html = "<p>Messages content</p>" });
            Contents.Add(new Content { Id = "16", PageId = "test", Title = "BeforeContent", Region = Region.BeforeContent, Html = "<p>BeforeContent content</p>" });
            Contents.Add(new Content { Id = "17", PageId = "test", Title = "Content", Region = Region.Content, Html = "<h2>Content</h2><p>Content content</p>" });
            Contents.Add(new Content { Id = "18", PageId = "test", Title = "AfterContent", Region = Region.AfterContent, Html = "<h2AfterContent</h2><p>AfterContent content</p>" });
            
            Contents.Add(new Content { Id = "19", PageId = "test", Title = "AfterMain", Region = Region.AfterMain, Html = "<p>AfterMain content</p>" });
           
            Contents.Add(new Content { Id = "20", PageId = "test", Title = "TripelFirst", Region = Region.TripelFirst, Html = "<p>TripelFirst content</p>" });
            Contents.Add(new Content { Id = "21", PageId = "test", Title = "TripelSecond", Region = Region.TripelSecond, Html = "<p>TripelSecond content</p>" });
            Contents.Add(new Content { Id = "21", PageId = "test", Title = "TripelThird", Region = Region.TripelThird, Html = "<p>TripelThird content</p>" });

            Contents.Add(new Content { Id = "23", PageId = "test", Title = "FooterQuadFirst", Region = Region.FooterQuadFirst, Html = "<p>FooterQuadFirst content</p>" });
            Contents.Add(new Content { Id = "24", PageId = "test", Title = "FooterQuadSecond", Region = Region.FooterQuadSecond, Html = "<p>FooterQuadSecond content</p>" });
            Contents.Add(new Content { Id = "25", PageId = "test", Title = "FooterQuadThird", Region = Region.FooterQuadThird, Html = "<p>FooterQuadThird content</p>" });
            Contents.Add(new Content { Id = "26", PageId = "test", Title = "FooterQuadFourth", Region = Region.FooterQuadFourth, Html = "<p>FooterQuadFourth content</p>" });
            Contents.Add(new Content { Id = "27", PageId = "test", Title = "Footer", Region = Region.Footer, Html = "<p>Footer content</p>" });
        }
    }
}